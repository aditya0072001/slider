﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    Rigidbody2D rb;
    public float moveSpeed;
    public float rotateAmount;
    public Outline glow;
    float rot;
    int score;
    public static int highscore;
    public Text winText;
    public Text highScore;
    public GameObject deathEffect;
    public AudioClip deathClip;
    static int loadCount = 0;
    string gameId = "3858647";
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        Advertisement.Initialize(gameId, false);
        score = 0;
        highscore = PlayerPrefs.GetInt("highscore", highscore);
        highScore.text = "High Score : "+highscore.ToString();
        winText.text = "Score : 0";

        if (loadCount % 3 == 0)  // only show ad every third time
        {
            ShowAd();
        }
        loadCount++;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
          
            if(mousePos.x < 0)
            {
                rot = rotateAmount;
            }
            else
            {
                rot = -rotateAmount;
            }
            
            transform.Rotate(0, 0, rot);
        }
        
        if (score > highscore)
        {
            highscore = score;
            winText.text = "Score : " + score;

            PlayerPrefs.SetInt("highscore", highscore);
        }
        glow.enabled = false;
    }
    private void FixedUpdate()
    {
        rb.velocity = transform.up * moveSpeed;
        //rb.MovePosition(rb.position + mousePos * moveSpeed * Time.fixedDeltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Food")
        {
            Destroy(collision.gameObject);
            Instantiate(deathEffect, transform.position, Quaternion.identity);
            AudioSource.PlayClipAtPoint(deathClip, transform.position);
            score++;
            winText.text = "Score : " + score;
            glow.enabled = true;      
        }
        else if (collision.gameObject.tag == "Danger")
        {
            //winText.SetActive(true);
            SceneManager.LoadScene("Menu");
        }
    }
    void OnDestroy()
    {
        PlayerPrefs.SetInt("highscore", highscore);
        PlayerPrefs.Save();
    }

    public void ShowAd()
    {
        if (Advertisement.IsReady())
        {
            Advertisement.Show();
        }
    }
}

