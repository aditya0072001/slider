﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFD : MonoBehaviour
{
    public GameObject danger;
    public GameObject food;
    public Vector3 center;
    public Vector3 size;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        SpawnDanger();
        SpawnFood();
    }

    public void SpawnDanger()
    {
        Vector3 pos = center + new Vector3(Random.Range(-size.x /2 , size.x /2), Random.Range(-size.y / 2, size.y / 2),0);

        Instantiate(danger,pos,Quaternion.identity);
    }

    public void SpawnFood()
    {
        Vector3 pos = center + new Vector3(Random.Range(-size.x / 2, size.x / 2), Random.Range(-size.y / 2, size.y / 2), 0);

        Instantiate(food, pos, Quaternion.identity);
    }
}
